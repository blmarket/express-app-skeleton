function routes(app) {
  app.get('/', function(req, res) {
    res.render('index.jade', { title: 'Sample Title' });
  });
}

module.exports.routes = routes;
